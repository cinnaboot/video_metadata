<?php
/*
 *	This file is part of video_metadata.
 *
 *	video_metadata is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *
 *	video_metadata is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with video_metadata.  If not, see <https://www.gnu.org/licenses/>.
 */


function getJsonErrorString($error_enum) : string
{
	$err = '';

	switch($error_enum) {
		case JSON_ERROR_NONE:
			$err = 'No error has occurred';
			break;
		case JSON_ERROR_DEPTH:
			$err = 'The maximum stack depth has been exceeded';
			break;
		case JSON_ERROR_STATE_MISMATCH:
			$err ='Invalid or malformed JSON';
			break;
		case JSON_ERROR_CTRL_CHAR:
			$err = 'Control character error, possibly incorrectly encoded';
			break;
		case JSON_ERROR_SYNTAX:
			$err = 'Syntax error';
			break;
		case JSON_ERROR_UTF8:
			$err = 'Malformed UTF-8 characters, possibly incorrectly encoded';
			break;
		case JSON_ERROR_RECURSION:
			$err = 'One or more recursive references in the value to be encoded';
			break;
		case JSON_ERROR_INF_OR_NAN:
			$err = 'One or more NAN or INF values in the value to be encoded';
			break;
		case JSON_ERROR_UNSUPPORTED_TYPE:
			$err = 'A value of a type that cannot be encoded was given';
			break;
		case JSON_ERROR_INVALID_PROPERTY_NAME:
			$err = 'A property name that cannot be encoded was given';
			break;
		case JSON_ERROR_UTF16:
			$err = 'Malformed UTF-16 characters, possibly incorrectly encoded';
			break;
	}

	return $err;
}

?>

<?php
/*
 *	This file is part of video_metadata.
 *
 *	video_metadata is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *
 *	video_metadata is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with video_metadata.  If not, see <https://www.gnu.org/licenses/>.
 */


require_once('getJsonErrorString.php');


class tmdb_interface
{
	const BASE_API_URL = 'https://api.themoviedb.org/3';


	public function read_header($ch, $str)
	{
		$header = explode(':', $str, 2);

		if (count($header) == 2) {
			$key = strtolower(trim($header[0]));
			$this->headers[$key] = trim($header[1]);
		}

		return strlen($str);
	}

	public function read_body($ch, $str)
	{
		$this->body_str = $str;
		$this->body_json = json_decode($str);
		$j_err = json_last_error();
		$this->query_duration = microtime(true) - $this->start_time;

		if ($j_err !== JSON_ERROR_NONE) {
			$this->errors[] = $str;
			$this->errors[] = __METHOD__ . ", JSON error: " . getJsonErrorString(json_last_error());
		}

		return strlen($str);
	}

	public function query($api_path, $api_key, $query_str)
	{
		$this->start_time = microtime(true);
		$this->request_str =
			tmdb_interface::BASE_API_URL .	'/' . $api_path .
			'?api_key=' . $api_key .
			'&query=' .  urlencode($query_str);

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $this->request_str);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLINFO_HEADER_OUT, true);
		curl_setopt($ch, CURLOPT_HEADERFUNCTION, array($this, 'read_header'));
		curl_setopt($ch, CURLOPT_WRITEFUNCTION, array($this, 'read_body'));
		$response = curl_exec($ch);
		$this->info = curl_getinfo($ch);
		$err = curl_error($ch);
		curl_close($ch);

		if ($err) {
			$this->errors[] = $err;
			return null;
		}

		$valid_reponse_codes = [200,304];

		if (!in_array($this->info['http_code'], $valid_reponse_codes, true)) {
			$this->errors[] = __METHOD__ . ", invalid response code: " . $this->info['http_code'];
			return null;
		}


		return $this->body_json;
	}


	public $request_str = "";
	public $headers = [];
	public $body_str = "";
	public $body_json;
	public $info;
	public $query_duration = 0;
	public $errors = [];


	private $start_time = 0;
}

?>

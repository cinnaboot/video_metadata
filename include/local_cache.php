<?php
/*
 *	This file is part of video_metadata.
 *
 *	video_metadata is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *
 *	video_metadata is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with video_metadata.  If not, see <https://www.gnu.org/licenses/>.
 */


class cache_result
{
	public $tmdb_id = 0;
	public $title = "";
	public $poster_path = "";
	public $backdrop_path = "";
	public $overview = "";
	public $release_date = "";
}

class cache_results
{
	public $cache_id = 0;
	public $search_str = "";
	public $result_count = 0;
	public $results = [];
}

class local_cache
{
	const DB_PATH = 'test.db';
	const MISSING_IMAGE = '_no_image_available.png';


	public function __construct()
	{
		$this->db = new SQLite3(local_cache::DB_PATH);
		// NOTE: need to enable foreign keys for every connection
		//	https://sqlite.org/foreignkeys.html#fk_enable
		$this->db->exec('pragma foreign_keys = on;');
	}

	public function __destruct()
	{
		$this->db->close();
	}

	public function search_movie_by_name($name)
	{
		$cache_id = $this->get_cache_id($name);

		if ($cache_id) {
			$stmt = $this->db->prepare('select * from query_results where cache_id=:c_id');
			$stmt->bindValue(':c_id', $cache_id, SQLITE3_INTEGER);
			$result = $stmt->execute();

			$crs = new cache_results();
			$crs->cache_id = $cache_id;
			$crs->search_str = $name;

			while ($row = $result->fetchArray(SQLITE3_ASSOC)) {
				$cr = new cache_result();
				$cr->tmdb_id = $row['tmdb_id'];
				$cr->title = $row['title'];
				$cr->poster_path = $row['poster_path'];
				$cr->backdrop_path = $row['backdrop_path'];
				$cr->overview = $row['overview'];
				$cr->release_date = $row['release_date'];
				$crs->results[$crs->result_count] = $cr;
				$crs->result_count++;
			}

			return $crs;
		}

		return null;
	}

	public function insert_query_cache($search_str, $tmdb_results)
	{
		$stmt = $this->db->prepare('insert into query_cache (query) values(:q)');
		$stmt->bindValue(':q', $search_str);
		$stmt->execute();
		$cache_id = $this->get_cache_id($search_str);

		$stmt = $this->db->prepare('insert into query_results'
			. '(tmdb_id, cache_id, title, poster_path, backdrop_path, overview, release_date)'
			. 'values'
			. '(:tmdb_id, :cache_id,  :title, :poster_path, :backdrop_path, :overview, :release_date)'
		);

		for ($i = 0; $i < count($tmdb_results->results); $i++) {
			$res = &$tmdb_results->results[$i];

			if ($res->poster_path == '')
				$res->poster_path = local_cache::MISSING_IMAGE;

			if ($res->backdrop_path == '')
				$res->backdrop_path = local_cache::MISSING_IMAGE;

			$stmt->bindValue(':tmdb_id', SQLite3::escapeString($res->id), SQLITE3_INTEGER);
			$stmt->bindValue(':cache_id', SQLite3::escapeString($cache_id), SQLITE3_INTEGER);
			$stmt->bindValue(':title', SQLite3::escapeString($res->title));
			$stmt->bindValue(':poster_path', SQLite3::escapeString(basename($res->poster_path)));
			$stmt->bindValue(':backdrop_path', SQLite3::escapeString(basename($res->backdrop_path)));
			$stmt->bindValue(':overview', SQLite3::escapeString($res->overview));
			$stmt->bindValue(':release_date', SQLite3::escapeString($res->release_date));
			$stmt->execute();
		}

		return $this->search_movie_by_name($search_str);
	}

	public function list_genres() {}

	# NOTE:
	# table movie_details: movie_id, title, overview, release_date, poster_path, backdrop_path
	# table movie_genre: genre_id, name
	# table movie_genre_mapping: genre_id, movie_id
	# table custom_tags: tag_id, name
	# table custom_tag_mapping: tag_id, movie_id
	# table actor_details: actor_id, name, date_of_birth, poster_path
	# table actor_movie_mapping: movie_id, actor_id, character_name
	public function insert_movie_details() {}

	# NOTE:
	# table tv_genre: genre_id, name
	# table tv_genre_mapping: genre_id, movie_id
	# table custom_tags: tag_id, name
	# table custom_tag_mapping: tag_id, movie_id
	# table actor_details: actor_id, name, date_of_birth, poster_path
	# table actor_tv_mapping: tv_id, actor_id, character_name
	public function insert_tv_details() {}

	public $errors = [];


	private function get_cache_id($search_str)
	{
		$stmt = $this->db->prepare('select * from query_cache where query=:q limit 1');
		$stmt->bindValue(':q', $search_str);
		$result = $stmt->execute();
		$row = $result->fetchArray(SQLITE3_ASSOC);

		if ($row)
			return $row['cache_id'];

		return null;
	}

	private $db;
}

?>

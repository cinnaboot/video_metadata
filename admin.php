<?php
/*
 *	This file is part of video_metadata.
 *
 *	video_metadata is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *
 *	video_metadata is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with video_metadata.  If not, see <https://www.gnu.org/licenses/>.
 */


declare(strict_types=1);
require_once('include/getJsonErrorString.php');


const FOLDER_PATH = '../video/unwatched';
//const FOLDER_PATH = '../video/tv.shows/Archer';
const MAX_RECURSE_DEPTH = 5;
$g_errors = [];
$g_debug = [];


class path_entry
{
	public $base_name = '';
	public $full_path = '';
	public $depth = 0;
	public $media_type = 'unknown'; // 'movie' or 'tv_show' or 'unknown'
	public $path_type = ''; // 'file' or 'directory'
	public $file_extension = '';
	public $content_size = 0;
	public $contents = []; // directory contents, array of path_entry objects
	public $parent = null; // reference to parent path_entry
}

function guessMediaType($filename) : string
{
	global $g_errors, $g_debug;
	$matches = [];
	$current_year = getdate()['year'];

	if (preg_match('/season?[0-9]{2}/i', $filename)) {
		return 'tv_show';
	} else if (preg_match('/s[0-9]{2}/i', $filename)) {
		return 'tv_show';
	} else if (preg_match_all('/[0-9]{4}/', $filename, $matches)) {
		foreach ($matches[0] as $match) {
			if ((int) $match >= 1878 && (int) $match <= $current_year)
				return 'movie';
		}
	}

	return 'unknown';
}

// NOTE: use parent reference to recursively set media types detected by children nodes
function setMediaType(&$path_entry, $media_type) : void
{
	// NOTE: don't set media type of top-level directory
	if ($path_entry->depth > 0) {
		$path_entry->media_type = $media_type;

		if ($path_entry->parent)
			setMediaType($path_entry->parent, $media_type);
	}
}

function getPathEntry($fs_path, $depth = 0, $media_type = '', &$parent = null) : ?path_entry
{
	$pe = new path_entry();
	$pe->base_name = basename($fs_path);
	$pe->full_path = $fs_path;
	$pe->depth = $depth;

	if ($parent !== null)
		$pe->parent =& $parent;

	if ($depth != 0 && ($media_type == '' || $media_type == 'unknown'))
		setMediaType($pe, guessMediaType($pe->base_name));
	else
		$pe->media_type = $media_type;

	if (is_dir($fs_path) && is_readable($fs_path)) {
		$pe->path_type = 'directory';

		if ($depth <= MAX_RECURSE_DEPTH) {
			foreach (scandir($fs_path) as $path) {
				if ($path != '.' && $path != '..') {
					$new_entry = getPathEntry($fs_path . '/' . $path, $depth + 1, $pe->media_type, $pe);

					if ($new_entry !== null)
						$pe->contents[] = $new_entry;
				}
			}
		}

		$pe->content_size = count($pe->contents);
	} else if (is_file($fs_path)) {
		$pe->path_type = 'file';
		$pe->file_extension = pathinfo($fs_path, PATHINFO_EXTENSION);

		if (!in_array($pe->file_extension, ['avi', 'mkv', 'm4v', 'mp4', 'mpg']))
			return null;
	} else {
		global $g_errors;
		$g_errors[] = __FUNCTION__ . ", invalid path: $fs_path";
		return null;
	}

	return $pe;
}

function unsetParentReferences(&$path_entry) : void
{
	unset($path_entry->parent);

	foreach ($path_entry->contents as &$entry)
		unsetParentReferences($entry);
}

function getUnsavedMetadata() : void
{
	$path_info = getPathEntry(FOLDER_PATH);
	global $g_errors, $g_debug;
	$out = new StdClass();

	header('Content-Type: text/json');

	if (count($g_errors) == 0) {
		unsetParentReferences($path_info); // NOTE: don't encode parent references
		$out->mem_usage = memory_get_peak_usage();
		$out->path_info = $path_info;
		$out->debug = $g_debug;
		$json = json_encode($out);

		if ($json) {
			echo $json;
		} else {
			echo '{"errors":"' . __FUNCTION__ . ', error enconding json: '
				. getJsonErrorString(json_last_error()) . '"}';
		}
	} else {
		$out->errors = $g_errors;
		echo json_encode($out);
	}
}

function verifyChanges() : void
{
	header('Content-Type: text/json');
	$input = file_get_contents('php://input');

	if ($input) {
		$json = json_decode($input);

		if ($json) {
			// TODO: recursively check that each path entry maps to a file on disk
			// 		and has a valid media type set
			// TODO: sanitize file names for TMDB queries
			// TODO: make queries to TMDB for sanitized titles
			// NOTE: this will be much easier for movies because TV shows will require
			// 		additional queries for seasons and episodes
			echo '{"errors": null, "message": "successfully decoded"}';
		} else {
			echo '{"errors":"' . __FUNCTION__ . ', error decoding json: '
				. getJsonErrorString(json_last_error()) . '"}';
		}
	}
}

function main() : void
{
	switch (@$_GET['action']) {
		case 'scan':
			getUnsavedMetadata();
			break;
		case 'file_map';
			//
			break;
		case 'verify':
			verifyChanges();
			break;
	}
}


main();

?>

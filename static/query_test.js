/*
 *	This file is part of video_metadata.
 *
 *	video_metadata is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *
 *	video_metadata is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with video_metadata.  If not, see <https://www.gnu.org/licenses/>.
 */


function responseHandler(data)
{
	let query_container = document.querySelector('#response_container');
	let info_container = document.querySelector('#info');
	let error_container = document.querySelector('#errors');
	let header_container = document.querySelector('#headers');
	let curl_info_container = document.querySelector('#curl_info');
	let json_container = document.querySelector('#json');

	resetContainers(info_container);

	let parser = new DOMParser();
	let doc = parser.parseFromString(data, "text/html");
	query_container.appendChild(doc.querySelector("#query_container"));
	info_container.appendChild(doc.querySelector("#info pre"));
	header_container.appendChild(doc.querySelector('#headers pre'));
	curl_info_container.appendChild(doc.querySelector('#curl_info pre'));
	json_container.appendChild(doc.querySelector('#json pre'));
	let errors = doc.querySelectorAll('#errors pre');

	if  (errors) {
		errors.forEach(function(element) {
			error_container.appendChild(element);
		});
	}
}

function onQuerySubmit(e)
{
	e.preventDefault();

	let query = e.target.querySelector('#tmdb_query').value;
	let url = '/video_metadata/query_page.php?tmdb_query=' + query;

	let query_container = document.querySelector('#response_container');
	let info_container = document.querySelector('#info');
	let error_container = document.querySelector('#errors');
	let header_container = document.querySelector('#headers');
	let curl_info_container = document.querySelector('#curl_info');
	let json_container = document.querySelector('#json');

	resetContainers(query_container, info_container, error_container,
		header_container, curl_info_container, json_container);

	info_container.appendChild(document.createTextNode('waiting for response...'));

	fetchQueryHTML(url, responseHandler);
}

window.onload = function () {
	if (!('fetch' in window)) {
		console.log('no fetch API');
		return;
	}

	let query_form = document.querySelector('#query_container form');
	query_form.addEventListener('submit', onQuerySubmit);
}

/*
 *	This file is part of video_metadata.
 *
 *	video_metadata is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *
 *	video_metadata is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with video_metadata.  If not, see <https://www.gnu.org/licenses/>.
 */


function fetchImage(url, domImg)
{
	fetch(url)
	.then(function(response) {
		if (!response.ok)
			throw Error(response.statusText);

		return response.blob();
	})
	.then(function(data) {
		var objectURL = URL.createObjectURL(data);
		domImg.src = objectURL;
		console.log(data);
	})
	.catch(function(error) {
		console.log('fetch error: ', error);
	});
}

function fetchQueryHTML(url, responseFunc)
{
	fetch(url)
	.then(function(response) {
		if (!response.ok)
			throw Error(response.statusText);

		return response.text();
	})
	.then(function(html) {
		responseFunc(html);
	})
	.catch(function(error) {
		console.log('fetch error: ', error);
		return null;
	});
}

function fetchJSON(url, responseFunc)
{
	fetch(url)
	.then(function(response) {
		if (!response.ok)
			throw Error(response.statusText);

		return response.json();
	})
	.then(function(json) {
		responseFunc(json);
	})
	.catch(function(error) {
		console.log('fetch error: ', error);
		return null;
	});
}

function fetchPOSTJSON(url, json, responseFunc)
{
	let init = {
			headers: {
				'Accept': 'application/json',
				'Content-Type': 'application/json'
			},
			method: 'POST',
			body: JSON.stringify(json)
		};

	let request = new Request(url, init);

	fetch(request)
	.then(function(response) {
		if (!response.ok)
			throw Error(response.statusText);

		return response.json();
	})
	.then(function(json) {
		responseFunc(json);
	})
	.catch(function(error) {
		console.log('fetch error: ', error);
		return null;
	});
}

function resetContainers(...args)
{
	args.forEach(function(arg) {
		while (arg.hasChildNodes())
			arg.removeChild(arg.firstChild);
	});
}


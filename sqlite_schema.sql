-- 	This file is part of video_metadata.
--
-- 	video_metadata is free software: you can redistribute it and/or modify
-- 	it under the terms of the GNU General Public License as published by
-- 	the Free Software Foundation, either version 3 of the License, or
-- 	(at your option) any later version.
--
-- 	video_metadata is distributed in the hope that it will be useful,
-- 	but WITHOUT ANY WARRANTY; without even the implied warranty of
-- 	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- 	GNU General Public License for more details.
--
-- 	You should have received a copy of the GNU General Public License
-- 	along with video_metadata.  If not, see <https://www.gnu.org/licenses/>.


CREATE TABLE movie_queries(
	query_id primary key not null autoimcrement,
	query text,
	year integer
);

CREATE TABLE movie_query_mapping(
	query text,
	query_id int,
	movie_id int
	foreign key(query_id) references movie_queries(query_id),
	foreign key(movie_id) references movie_details(movie_id)
);

CREATE TABLE movie_details(
	movie_id primary key not null,
	title text,
	overview text,
	release_date date,
	poster_path text
);

CREATE TABLE movie_genres(
	genre_id integer primary key not null,
	genre_name
);

CREATE TABLE movie_genre_mapping(
	genre_id integer,
	movie_id integer,
	foreign key(genre_id) references movie_genres(genre_id),
	foreign key(movie_id) references movie_details(movie_id)
);

CREATE TABLE actor_details(
	actor_id integer primary key not null,
	name text,
	date_of_birth date,
	poster_path text
);

CREATE TABLE actor_movie_mapping(
	movie_id integer,
	actor_id integer,
	character_name text,
	foreign key(movie_id) references movie_details(movie_id),
	foreign key(actor_id) references actor_details(actor_id)
);


-- TODO: tv shows

CREATE TABLE tv_queries(
	query text,
	year integer
);

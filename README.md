# video_metadata

## requirements
- web server with php support (testing with lighttpd and fast-cgi)
- PHP > 7
- sqlite3
- browser that supports fetch API

## installation
- aquire an api-key from https://developers.themoviedb.org/3/getting-started/introduction
- add key to 'include/api_key.php' as `$API_KEY = 'your key hash';`
- edit 'FOLDER_PATH' in 'admin.php' to the location of your video files
  - NOTE: your webserver must have read access to this folder
- import schema into new database file with `$ sqlite3 DB_NAME.db < sqlite_schema.sql`
- update DB_PATH in 'include/local_cache.php' to match db filename

<?php
/*
 *	This file is part of video_metadata.
 *
 *	video_metadata is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *
 *	video_metadata is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with video_metadata.  If not, see <https://www.gnu.org/licenses/>.
 */


require('include/api_key.php');
require('include/tmdb_interface.php');
require('include/local_cache.php');


function log_errors($args)
{
	echo "<pre>";

	foreach (func_get_args() as $arg)
		print_r($arg);

	echo "</pre>";
}

function sanitize($str)
{
	$verboten = ['/',"\\",'\'','"'];
	$str = str_replace($verboten, '', $str);
	$str = trim(str_replace('.', ' ', strtolower($str)));

	return $str;
}

function main(&$local_cache, &$tmdb_interface, &$cache_results, &$is_cached, $query_str, $key)
{
	if ($query_str == '')
		return;

	$cache_results = $local_cache->search_movie_by_name($query_str);
	$is_cached = ($cache_results !== null);

	if (!$is_cached) {
		$parsed = $tmdb_interface->query('search/movie', $key, $query_str);

		if ($tmdb_interface->errors) {
			foreach ($tmdb_interface->errors as $err) {
				error_log(__FILE__ . " , " . __FUNCTION__ . " , $err");
			}
		} else if ($parsed) {
			$cache_results = $local_cache->insert_query_cache($query_str, $parsed);
		}
	}
}


$raw_str = @$_GET['tmdb_query'];
$query_str = sanitize($raw_str);

$lc = new local_cache();
$tmdb = new tmdb_interface();
$data = null;
$is_cached = false;

main($lc, $tmdb, $data, $is_cached, $query_str, $API_KEY);

?>
<!DOCTYPE html>
<html>
<body>

<div id="info">
	<pre>
time: "<?php echo date(DATE_RSS); ?>"
timestamp: "<?php echo time(); ?>"
key: "<?php echo $API_KEY; ?>"
raw str: "<?php echo $raw_str; ?>"
query str: "<?php echo $query_str; ?>"
request str: "<?php echo $tmdb->request_str; ?>"
cache hit: "<?php echo ($is_cached) ? "true" : "false"; ?>"
query_duration: "<?php echo $tmdb->query_duration; ?>s"
	</pre>
</div>

<div id="errors">
<?php
	if (!empty($lc->errors))
		log_errors($lc_errors);

	if (!empty($tmdb->errors))
		log_errors($tmdb->errors);
?>
</div>

<div id="headers">
	<pre><?php print_r($tmdb->headers); ?></pre>
</div>

<div id="curl_info">
	<pre><?php print_r($tmdb->info); ?></pre>
</div>

<div id="json">
	<pre><?php print_r($tmdb->body_json); ?></pre>
</div>

<h3>query page test</h3>
<div id="query_container">
<?php
if ($data) {
	foreach ($data->results as $movie) {
		echo '	<div class="query_item">';

		if ($movie->poster_path) {
			echo '		<img src="image_cache.php'
				. '?image_url=' . basename($movie->poster_path)
				. '&amp;image_type=poster" />';
		}
?>
		<ul class="query_details">
			<li><span>title: <?php echo $movie->title; ?></span></li>
			<li><span>released: <?php echo $movie->release_date; ?></span></li>
			<li><span>description: <?php echo $movie->overview; ?></span></li>
		</ul>
	</div>
<?php }} ?>
</div>

</body>
